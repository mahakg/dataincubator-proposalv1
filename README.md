       ## Terrorism is defined by the nature of the act, not by the identity of the perpetrators or the nature of the cause ##-- RAND Corporation

# Title: 

Analysis of Global Terrorism Incidents 

# Overview:

The overall goal of this project is to analyse the Global Terrorism Incidents (GTD) in past to generate insights from already collected data. The scope of project also includes to link the insights generated from terrorism events with GDP growth, unemployment rate (more factors to add; available from IMF data) of the countries which are at worse with the problem of terrorism (see plot 1 below). The terrorism in these countries can also be linked with the import data of Arms and Ammunitions (SIPRI Arms Transfers Database). Apart from these analysis, I'm also motivated to work on a thorough analysis of terrorism in India (mainly focussed on maoists and naxalites as this is one of biggest challenge to deal with for Government in some states).

# Data Sources:
1. Global Terrorism Data: It is comprised of more than 150k terrorism incidents globally from the span of 1970-2016. My analysis are primarily based on this dataset. The data is available at http://www.start.umd.edu/gtd/

2. IMF data: World Economic Outlook database. This lists the index of unemployment rate, GDP growth, exports, imports etc. The dataset is available at http://www.imf.org/external/pubs/ft/weo/2016/01/weodata/download.aspx.

3. SIPRI Arms Transfers Database - The SIPRI Arms Transfers Database contains information on all transfers of major conventional weapons from 1950 to the most recent full calendar year. The data is available for one country at a time; https://www.sipri.org/databases/armstransfers. I've to scrape the website and download the files for a specific country.

# Exploratory Data Analysis:

1. Total number of Incidents from 1970 - 2016

Insight: Clearly 2013-2015 has been a very bad phase in the perspect of terrorism; It is primarily due to ISIS situation

![terror_incidents.png](https://bitbucket.org/repo/GqA69y/images/1455486068-terror_incidents.png)

2. Most affected countries by terrorism (last 5 years)

Insight: As expected the situation in Iraq is severely worse. The primary reason for this is Iraqi civil war. The situation in Pakistan, Afghanistan and India is not that good, primarily due to the presence of organisations like Taliban, Al-Qaeda and Maoists activities.

![worse_countries_situation.png](https://bitbucket.org/repo/GqA69y/images/316524284-worse_countries_situation.png)

3. Leading terrorists organisations in past years

![terror_groups_5 years.png](https://bitbucket.org/repo/GqA69y/images/3081392903-terror_groups_5%20years.png)

4. Weapon types used in past 5 years

Insight : Explosives, Dynamites, Bombs are having the majority share in this ecosystem. 

![weapon_type_used.png](https://bitbucket.org/repo/GqA69y/images/3263508324-weapon_type_used.png)

5. Casulalities comparison on weapon type used (Injured vs fatalities)

![fatalities.png](https://bitbucket.org/repo/GqA69y/images/405952476-fatalities.png)

6. Terror Index with GDP

Insight: the graph (focussed on Afghanistan) shows that as the trend of GDP values increases i.e more development in the country; terror incidents are likely to get reduced. It would be interesting to see the impact of terrorist incidents with the unemployment rate; Poverty Index etc. These will be covered in the future scope of this project.

![Terrorindex with gdp.png](https://bitbucket.org/repo/GqA69y/images/985640537-Terrorindex%20with%20gdp.png)