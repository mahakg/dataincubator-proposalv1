import pandas as pd

data = pd.read_csv('ml-uni/sfo-data.csv', sep=';', usecols=[
                                                            'Property Location', 'Neighborhood Code',
                                                            'Block and Lot Number', 'Property Class Code',
                                                            'Closed Roll Assessed Improvement Value',
                                                            'Year Property Built',
                                                            'Number of Units'
                                                            ])

data.fillna(0, inplace=True)

gp =  data.groupby('Property Class Code').size().reset_index(name='count')

sorted_class_count =  gp.sort_values('count', ascending=False)

print "Most common class is '", sorted_class_count['Property Class Code'].iloc[0] , "'. Its fraction of total assessment is :", ((sorted_class_count['count'].iloc[0]/ float(data.shape[0])))

improvement_vals = data[data['Closed Roll Assessed Improvement Value']!=0]
print "Median value of assessed improved value is ", improvement_vals['Closed Roll Assessed Improvement Value'].median()


print "average assessment impovement value is ", improvement_vals['Closed Roll Assessed Improvement Value'].mean()
print "Difference between "


gp =  data.groupby('Block and Lot Number').size().reset_index(name='count')

block_count =  gp.sort_values('count', ascending=False)

data_drop_duplicate_properties = data.drop_duplicates(subset=['Property Location', 'Neighborhood Code'])

num_units_less_1950 = data_drop_duplicate_properties[(data_drop_duplicate_properties['Year Property Built'] !=0) & (data_drop_duplicate_properties['Year Property Built'] <1950)]

print num_units_less_1950['Number of Units'].sum()/float(num_units_less_1950['Number of Units'].shape[0])

# print num_units_less_1950['Number of Units'].shape
num_units_greater_1950 = data_drop_duplicate_properties[(data_drop_duplicate_properties['Year Property Built'] !=0) & (data_drop_duplicate_properties['Year Property Built'] >=1950)]


print "Difference in number of units is :", num_units_greater_1950['Number of Units'].sum()/float(num_units_greater_1950['Number of Units'].shape[0])
# print num_units_less_1950['Number of Units'].shape






