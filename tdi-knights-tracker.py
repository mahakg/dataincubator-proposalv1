# import random package
import random
import numpy


float_formatter = lambda x: "%.10f" % x
# track moves of all number pad digits in array indexes i.e the index 0 in this
# list keeps the possible moves when the key is 0

moves = [
         [4,6],
         [6,8],
         [7,9],
         [4,8],
         [0,3,9],
         [],
         [0,1,7],
         [2,6],
         [1,3],
         [2,4]
         ]

"""
This function returns the sum of moves by knight. It accepts two variables
T : The number of moves allowed
start : Customisable value to start the move
"""

def knight_moves(T, start = 0):

    # sum of keys where the knight lands
    sum_keys = 0

    # list to keep track of moves by knight.
    track_moves = []

    expectation = 0



    # loop from T to 0
    for item in range(T, 0, -1):

        # No possible moves will be the case of '5'
        if len(moves[start]) == 0:
            break

        randindex = numpy.random.randint(0, len(moves[start]))

        # randomly get a key from available choices
        key = moves[start][randindex]
        print moves[start], '----', key

        # update the start


        # track sum
        sum_keys = sum_keys + key
        #print sum(moves[start]), '---', (sum(moves[start])/float(len(moves[start]) ))
        expectation = expectation + (sum(moves[start])/len(moves[start]) )

        # add current key to track_moves
        track_moves.append(key)
        start = key

    # print track_moves
    # print numpy.mean(track_moves)
    print sum_keys
    print expectation

    return sum_keys % T, float_formatter(numpy.std(track_moves))


print knight_moves(5, 0)
#print knight_moves(1024, 0)