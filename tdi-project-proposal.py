import pandas as pd
import re
import matplotlib.pyplot as plt

data = pd.read_csv('ml-uni/globalterrorismdb_0616dist.csv', sep=';', usecols=[
                                                            'iyear', 'region', 'region_txt', 'attacktype1_txt',
    'targtype1_txt','weaptype1_txt','success', 'gname', 'country_txt'

                                                            ])

# print data.groupby(['iyear', 'targtype1_txt']).size().head(5)
x=  data[data['iyear'] > 2012].groupby(['weaptype1_txt']).size().reset_index(name='Number_Of_Terror_Incidents')

x.sort_values(by=['Number_Of_Terror_Incidents'], ascending=False, inplace=True)


# x[0:10].plot(x='weaptype1_txt', y='Number_Of_Terror_Incidents', kind='bar',color='k', title='Terrorist activities by various groups in last 5 years')
print x[0:10]
#plt.show()

df = pd.read_csv('ml-uni/globalterrorismdb_0616dist.csv', sep=';', low_memory=False, nrows=1000)
regex = re.compile('[^a-zA-Z\s]')

text_cols = ['summary','alternative_txt','attacktype1_txt','attacktype2_txt','attacktype3_txt','targtype1_txt','targsubtype1_txt','natlty1_txt','targtype2_txt','targsubtype2_txt','natlty2_txt','targtype3_txt','targsubtype3_txt','natlty3_txt','motive','guncertain1','guncertain2','guncertain3','nperps','nperpcap','claimmode_txt','claimmode2_txt','claimmode3_txt','compclaim','weaptype1_txt','weapsubtype1_txt','weaptype2_txt','weapsubtype2_txt','weaptype3_txt','weapsubtype3_txt','weaptype4_txt','weapsubtype4_txt','weapdetail','propextent_txt','propcomment','ishostkid','nhostkid','nhostkidus','divert','kidhijcountry','ransom','ransomnote','hostkidoutcome_txt','addnotes','scite1','scite2','scite3','related']

df[text_cols] = df[text_cols].fillna('')

df['full_text'] = df[text_cols].apply(lambda x: ' '.join(str(x)), axis=1)

df.full_text = df.full_text.apply(lambda x: regex.sub(' ', x.lower()).replace('  ', ' ').replace('  ', ' '))

print df.full_text


#data.groupby(data['date'].map(lambda x: x.year))

by_year = df.groupby(df['iyear'].map(lambda x: x.year))['full_text'].apply(lambda x: ' '.join(x))

#df_first['word_count'] = len(df_first['full_text'])

#df_first.loc[1980] #.split()

print by_year